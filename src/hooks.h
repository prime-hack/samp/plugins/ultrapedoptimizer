#pragma once

#include <functional>

namespace game {
	struct CPed;
	struct CPlayerPed;
} // namespace game

namespace hooks {
	extern std::function<bool( game::CPed * )> is_need_process_ped;
	extern std::function<void()> on_render;
	extern std::function<void( game::CPed * )> on_before_update_attachments;
	extern std::function<void()> on_after_update_attachments;
	extern std::function<void( game::CPlayerPed * )> update_player_pad;
} // namespace hooks
