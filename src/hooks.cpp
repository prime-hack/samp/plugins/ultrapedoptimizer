#include "hooks.h"

#include <stdexcept>

#include <MinHook.h>

#include "game.hpp"

#define INIT_STATIC( name ) decltype( name ) name

INIT_STATIC( hooks::is_need_process_ped );
INIT_STATIC( hooks::on_render );
INIT_STATIC( hooks::on_before_update_attachments );
INIT_STATIC( hooks::on_after_update_attachments );
INIT_STATIC( hooks::update_player_pad );

#undef INIT_STATIC

namespace hooks {
	static bool is_need_process_entity( game::CEntity *entity ) {
		if ( entity->GetType() != game::ENTITY_TYPE::ENTITY_TYPE_PED ) return true;
		return hooks::is_need_process_ped( static_cast<game::CPed *>( entity ) );
	}
} // namespace hooks

namespace {
	bool render_textdraws = false;

	namespace addr {
		struct address_t {
			std::uintptr_t value = 0;

			consteval address_t( std::uintptr_t address ) : value( address ) {}

			template<typename T> operator T *() const { return reinterpret_cast<T *>( value ); }
		};

		consteval address_t operator""_addr( unsigned long long int value ) {
			return { static_cast<std::uintptr_t>( value ) };
		}

		namespace CEntity {
			static constexpr auto UpdateAnim = 0x535F00_addr;
			static constexpr auto UpdateRwFrame = 0x532B00_addr;
		} // namespace CEntity

		namespace CPhysical {
			static constexpr auto ProcessCollision = 0x54DFB0_addr;
			static constexpr auto ProcessShift = 0x54DB10_addr;
		} // namespace CPhysical

		namespace CPed {
			static constexpr auto PreRender = 0x5E8A20_addr;
			static constexpr auto PreRenderAfterTest = 0x5E65A0_addr;
			static constexpr auto Render = 0x5E7680_addr;
			static constexpr auto GetTransformedBonePosition = 0x5E01C0_addr;
		} // namespace CPed

		namespace CPlayerPed {
			static constexpr auto ProcessControl = 0x60EA90_addr;
		} // namespace CPlayerPed

		namespace CSpecialFX {
			static constexpr auto Render = 0x726AD0_addr;
		} // namespace CSpecialFX

		namespace CPedIntelligence {
			static constexpr auto ProcessAfterPreRender = 0x6019B0_addr;
		} // namespace CPedIntelligence

		namespace CMirrors {
			static constexpr auto BeforeMainRender = 0x727140_addr;
		} // namespace CMirrors

		static constexpr auto Idle = 0x53E920_addr;
		static constexpr auto SetLightsWithTimeOfDayColour = 0x7354E0_addr;
	} // namespace addr

	namespace o {
		namespace CEntity {
			void *UpdateAnim;
			void *UpdateRwFrame;
		} // namespace CEntity

		namespace CPhysical {
			void *ProcessCollision;
			void *ProcessShift;
		} // namespace CPhysical

		namespace CPed {
			void *PreRender;
			void *PreRenderAfterTest;
			void *Render;
			void *GetTransformedBonePosition;
		} // namespace CPed

		namespace CPlayerPed {
			void *ProcessControl;
		} // namespace CPlayerPed

		namespace CSpecialFX {
			void *Render;
		} // namespace CSpecialFX

		namespace CPedIntelligence {
			void *ProcessAfterPreRender;
		} // namespace CPedIntelligence

		namespace CMirrors {
			void *BeforeMainRender;
		} // namespace CMirrors

		void *Idle;
		void *SetLightsWithTimeOfDayColour;
	} // namespace o

	namespace hk {
#define ORIG_FN( func ) reinterpret_cast<decltype( &( func ) )>( o::func )

		namespace CEntity {
			void __fastcall UpdateAnim( game::CEntity *This, void *EDX ) {
				if ( !hooks::is_need_process_entity( This ) ) return;
				return ORIG_FN( CEntity::UpdateAnim )( This, EDX );
			}

			void __fastcall UpdateRwFrame( game::CEntity *This, void *EDX ) {
				if ( !hooks::is_need_process_entity( This ) ) return;
				return ORIG_FN( CEntity::UpdateRwFrame )( This, EDX );
			}
		} // namespace CEntity

		namespace CPhysical {
			void __fastcall ProcessCollision( game::CPhysical *This, void *EDX ) {
				if ( !hooks::is_need_process_entity( This ) ) return;
				return ORIG_FN( CPhysical::ProcessCollision )( This, EDX );
			}

			void __fastcall ProcessShift( game::CPhysical *This, void *EDX ) {
				if ( !hooks::is_need_process_entity( This ) ) return;
				return ORIG_FN( CPhysical::ProcessShift )( This, EDX );
			}
		} // namespace CPhysical

		namespace CPed {
			void __fastcall PreRender( game::CPed *This, void *EDX ) {
				if ( !render_textdraws && !hooks::is_need_process_ped( This ) ) return;
				return ORIG_FN( CPed::PreRender )( This, EDX );
			}

			void __fastcall PreRenderAfterTest( game::CPed *This, void *EDX ) {
				if ( !render_textdraws && !hooks::is_need_process_ped( This ) ) return;
				return ORIG_FN( CPed::PreRenderAfterTest )( This, EDX );
			}

			void __fastcall Render( game::CPed *This, void *EDX ) {
				if ( !render_textdraws && !hooks::is_need_process_ped( This ) ) return;
				return ORIG_FN( CPed::Render )( This, EDX );
			}

			void __fastcall GetTransformedBonePosition( game::CPed *This,
														void *EDX,
														game::CVector *posn,
														int boneTag,
														bool bCalledFromCamera ) {
				if ( !hooks::is_need_process_ped( This ) ) {
					*posn = This->m_pMat->Project( game::aStdBonePosisions[boneTag] );
					return;
				}
				return ORIG_FN( CPed::GetTransformedBonePosition )( This, EDX, posn, boneTag, bCalledFromCamera );
			}
		} // namespace CPed

		namespace CPlayerPed {
			void __fastcall ProcessControl( game::CPlayerPed *This, void *EDX ) {
				hooks::update_player_pad( This );
				if ( !hooks::is_need_process_ped( This ) ) {
					This->ApplySpeed();
					return;
				}
				return ORIG_FN( CPlayerPed::ProcessControl )( This, EDX );
			}
		} // namespace CPlayerPed

		namespace CSpecialFX {
			void __cdecl Render() {
				hooks::on_render();
				return ORIG_FN( CSpecialFX::Render )();
			}
		} // namespace CSpecialFX

		namespace CPedIntelligence {
			void __fastcall ProcessAfterPreRender( game::CPedIntelligence *This, void *EDX ) {
				ORIG_FN( CPedIntelligence::ProcessAfterPreRender )( This, EDX );
				hooks::on_before_update_attachments( This->m_pPed );
			}
		} // namespace CPedIntelligence

		namespace CMirrors {
			void __cdecl BeforeMainRender() {
				hooks::on_after_update_attachments();
				return ORIG_FN( CMirrors::BeforeMainRender )();
			}
		} // namespace CMirrors

		void __cdecl Idle( void *param, bool firstFrame ) {
			render_textdraws = true;
			return ORIG_FN( Idle )( param, firstFrame );
		}

		void __cdecl SetLightsWithTimeOfDayColour( game::RpWorld *world ) {
			render_textdraws = false;
			return ORIG_FN( SetLightsWithTimeOfDayColour )( world );
		}

#undef ORIG_FN
	} // namespace hk

	template<addr::address_t addr> struct hook final {
		template<typename FuncSig> hook( FuncSig &&Fn, void *&orig ) {
			if ( MH_CreateHook( addr, reinterpret_cast<void *>( &Fn ), &orig ) != MH_OK )
				throw std::runtime_error( "MH_CreateHook failed" );
			MH_EnableHook( addr );
		}

		~hook() {
			MH_DisableHook( addr );
			MH_RemoveHook( addr );
		}
	};

	struct minhook_initializer {
		minhook_initializer() {
			if ( MH_Initialize() != MH_OK ) throw std::runtime_error( "MH_Initialize failed" );
		}

		~minhook_initializer() { MH_Uninitialize(); }
	};

#define __DEF_HOOK( name, func ) /*NOLINT*/ \
	hook<addr::func> hook_##name { \
		hk::func, o::func \
	}

#define __DEF_HOOK_WRAPPER( name, func ) /*NOLINT*/ __DEF_HOOK( name, func )

#define DEF_HOOK( func ) __DEF_HOOK_WRAPPER( __COUNTER__, func )

	struct hooks_loader final : minhook_initializer {
		DEF_HOOK( CEntity::UpdateAnim );
		DEF_HOOK( CEntity::UpdateRwFrame );
		DEF_HOOK( CPhysical::ProcessCollision );
		DEF_HOOK( CPhysical::ProcessShift );
		DEF_HOOK( CPed::PreRender );
		DEF_HOOK( CPed::PreRenderAfterTest );
		DEF_HOOK( CPed::Render );
		DEF_HOOK( CPed::GetTransformedBonePosition );
		DEF_HOOK( CPlayerPed::ProcessControl );
		DEF_HOOK( CSpecialFX::Render );
		DEF_HOOK( CPedIntelligence::ProcessAfterPreRender );
		DEF_HOOK( CMirrors::BeforeMainRender );
		DEF_HOOK( Idle );
		DEF_HOOK( SetLightsWithTimeOfDayColour );
	} loader;

#undef DEF_HOOK
#undef __DEF_HOOK_WRAPPER
#undef __DEF_HOOK
} // namespace