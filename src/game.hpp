#pragma once

#include <cmath>
#include <cstddef>
#include <cstdint>

namespace game {
	struct CVector {
		float x = 0.0f;
		float y = 0.0f;
		float z = 0.0f;

		inline float MagnitudeSqr() const { return x * x + y * y + z * z; }

		inline float Magnitude() const { return std::sqrt( MagnitudeSqr() ); }

#define VEC_OP( op ) \
	inline CVector operator op( const CVector &rhs ) const { \
		return { x op rhs.x, y op rhs.y, z op rhs.z }; \
	} \
	inline CVector operator op( float value ) const { \
		return { x op value, y op value, z op value }; \
	} \
	inline CVector &operator op##=( const CVector &rhs ) { \
		x op## = rhs.x; \
		y op## = rhs.y; \
		z op## = rhs.z; \
		return *this; \
	} \
	inline CVector &operator op##=( float value ) { \
		x op## = value; \
		y op## = value; \
		z op## = value; \
		return *this; \
	}

		VEC_OP( +)
		VEC_OP( -)
		VEC_OP( * )
		VEC_OP( / )

#undef VEC_OP
	};

	struct RwMatrix {
		CVector right;
		float pad1;
		CVector up;
		float pad2;
		CVector at;
		float pad3;
		CVector pos;
		float pad4;

		inline CVector Project( CVector vec ) const {
			CVector out = pos;
			out.x += at.x * vec.z + up.x * vec.y + right.x * vec.x;
			out.y += at.y * vec.z + up.y * vec.y + right.y * vec.x;
			out.z += at.z * vec.z + up.z * vec.y + right.z * vec.x;
			return out;
		}
	};

	struct CMatrix : RwMatrix {
		struct RwMatrix *m_pRwMat{ nullptr };
		std::uint32_t m_owner{ 0 };
	};

	struct CPlaceable {
		static constexpr auto kVtblAddr = 0x863C40;
		std::uintptr_t vtbl;
		struct CVector m_translate;
		float m_heading;
		CMatrix *m_pMat;

		inline CVector GetPosition() const {
			if ( m_pMat != nullptr ) return m_pMat->pos;
			return m_translate;
		}

		inline float GetHeading() const {
			if ( m_pMat != nullptr ) return std::atan2( -m_pMat->up.x, m_pMat->up.y );
			return m_heading;
		}
	};

	enum ENTITY_TYPE {
		ENTITY_TYPE_NOTHING = 0,
		ENTITY_TYPE_BUILDING = 1,
		ENTITY_TYPE_VEHICLE = 2,
		ENTITY_TYPE_PED = 3,
		ENTITY_TYPE_OBJECT = 4,
		ENTITY_TYPE_DUMMY = 5,
		ENTITY_TYPE_NOTINPOOLS = 6,
	};

	struct CEntityInfo {
		std::uint8_t nType	 : 3;
		std::uint8_t nStatus : 5;
	};

	struct CEntity : CPlaceable {
		std::uint8_t pad[0x1e];
		CEntityInfo m_info;

		inline ENTITY_TYPE GetType() const { return static_cast<ENTITY_TYPE>( m_info.nType ); }
	};

	struct CPhysical : CEntity {
		inline void ApplySpeed() const {
			static constexpr auto kApplySpeedAddr = 0x547B80;
			using fn_t = void( __thiscall * )( const CEntity * );
			auto fn = reinterpret_cast<fn_t>( kApplySpeedAddr );

			return fn( this );
		}
	};

	struct CPed : CPhysical {
		char pad[0x4f8];
		std::uint32_t state;
		char pad2[0xc];
		float m_nHealth;
		char pad3[0x1d4];
		std::uint8_t m_nCurrentWeapon;

		inline bool IsDead() const { return state == 55 || state == 63 || m_nHealth <= 0.f; }

		inline bool IsPlayer() const {
			static constexpr auto kIsPlayerAddr = 0x5DF8F0;
			using fn_t = bool( __thiscall * )( const CPed * );
			auto fn = reinterpret_cast<fn_t>( kIsPlayerAddr );

			return fn( this );
		}

		inline bool IsArmsMeleeWeapon() const {
			return m_nCurrentWeapon == 0 || m_nCurrentWeapon == 1 || m_nCurrentWeapon == 9 || m_nCurrentWeapon == 10 ||
				   m_nCurrentWeapon == 11 || m_nCurrentWeapon == 12;
		}
	};

	struct CPlayerPed : CPed {
		static inline CPlayerPed *local() {
			static constexpr auto kLocalPlayerAddr = 0xB6F5F0;

			return *reinterpret_cast<CPlayerPed **>( kLocalPlayerAddr );
		}
	};

	struct CPedIntelligence {
		CPed *m_pPed;
	};

	template<class T> struct CPool {
		struct CFlag {
			std::uint8_t index	 : 7;
			std::uint8_t is_free : 1;
		};

		T *m_aStorage;
		CFlag *m_aFlags;
		std::int32_t m_nSize;
		std::int32_t m_nFreeIndex;
		bool m_bOwnsArrays;
		bool m_bDealWithNoMemory;
	};

	template<> struct CPool<CPed> : CPool<void> {
		static constexpr auto kDefaultPoolSize = 140;
		static constexpr auto kPedPoolAddr = 0xB74490;

		static inline CPool<CPed> *get() { return *reinterpret_cast<CPool **>( kPedPoolAddr ); }

		inline std::size_t entry_size() const {
			static constexpr auto kAtIndexAddr = 0x4082A0;

			static std::size_t calculated_size = 0;
			if ( calculated_size != 0 ) return calculated_size;

			auto atIndex = reinterpret_cast<CPed *(__thiscall *)( const CPool<CPed> *, std::int32_t )>( kAtIndexAddr );
			for ( auto i = 0; i < m_nSize - 1; i++ ) {
				if ( m_aFlags[i].is_free || m_aFlags[i + 1].is_free ) continue;
				auto *lhs = atIndex( this, i + 1 );
				auto *rhs = atIndex( this, i );
				calculated_size = reinterpret_cast<std::uintptr_t>( lhs ) - reinterpret_cast<std::uintptr_t>( rhs );
				break;
			}
			return calculated_size;
		}
	};

	enum RwRenderState {
		rwRENDERSTATENARENDERSTATE = 0,
		rwRENDERSTATETEXTURERASTER,
		rwRENDERSTATETEXTUREADDRESS,
		rwRENDERSTATETEXTUREADDRESSU,
		rwRENDERSTATETEXTUREADDRESSV,
		rwRENDERSTATETEXTUREPERSPECTIVE,
		rwRENDERSTATEZTESTENABLE,
		rwRENDERSTATESHADEMODE,
		rwRENDERSTATEZWRITEENABLE,
		rwRENDERSTATETEXTUREFILTER,
		rwRENDERSTATESRCBLEND,
		rwRENDERSTATEDESTBLEND,
		rwRENDERSTATEVERTEXALPHAENABLE,
		rwRENDERSTATEBORDERCOLOR,
		rwRENDERSTATEFOGENABLE,
		rwRENDERSTATEFOGCOLOR,
		rwRENDERSTATEFOGTYPE,
		rwRENDERSTATEFOGDENSITY,
		rwRENDERSTATECULLMODE = 20,
		rwRENDERSTATESTENCILENABLE,
		rwRENDERSTATESTENCILFAIL,
		rwRENDERSTATESTENCILZFAIL,
		rwRENDERSTATESTENCILPASS,
		rwRENDERSTATESTENCILFUNCTION,
		rwRENDERSTATESTENCILFUNCTIONREF,
		rwRENDERSTATESTENCILFUNCTIONMASK,
		rwRENDERSTATESTENCILFUNCTIONWRITEMASK,
		rwRENDERSTATEALPHATESTFUNCTION,
		rwRENDERSTATEALPHATESTFUNCTIONREF,
		rwRENDERSTATEFORCEENUMSIZEINT = 2147483647
	};

	enum RwCullMode {

		rwCULLMODENACULLMODE = 0,
		rwCULLMODECULLNONE,
		rwCULLMODECULLBACK,
		rwCULLMODECULLFRONT,
		rwCULLMODEFORCEENUMSIZEINT = 2147483647
	};

	template<typename T> inline void SetRenderState( std::uint32_t state, T value ) {
		auto rwInstance = *reinterpret_cast<std::uintptr_t *>( 0xC97B24 );
		auto fn_addr = *reinterpret_cast<std::uintptr_t *>( rwInstance + 0x20 );
		using fn_type = void( __cdecl * )( std::uint32_t, T );
		auto fn = reinterpret_cast<fn_type>( fn_addr );
		return fn( state, value );
	}

	template<typename T> inline T GetRenderState( std::uint32_t state ) {
		auto rwInstance = *reinterpret_cast<std::uintptr_t *>( 0xC97B24 );
		auto fn_addr = *reinterpret_cast<std::uintptr_t *>( rwInstance + 0x24 );
		using fn_type = void( __cdecl * )( std::uint32_t, T * );
		auto fn = reinterpret_cast<fn_type>( fn_addr );

		T result;
		fn( state, &result );
		return result;
	}

	struct RwRGBA {
		std::uint8_t red;
		std::uint8_t green;
		std::uint8_t blue;
		std::uint8_t alpha;
	};

	constexpr auto kRwRGBAWhite = RwRGBA{ 255, 255, 255, 255 };

	struct RwTexCoords {
		float u;
		float v;
	};

	struct RwIm3DVertex {
		CVector position;
		CVector normal;
		RwRGBA color;
		RwTexCoords texCoords;
	};

	enum RwPrimitiveType {
		rwPRIMTYPENAPRIMTYPE = 0,
		rwPRIMTYPELINELIST,
		rwPRIMTYPEPOLYLINE,
		rwPRIMTYPETRILIST,
		rwPRIMTYPETRISTRIP,
		rwPRIMTYPETRIFAN,
		rwPRIMTYPEPOINTLIST,
		rwPRIMITIVETYPEFORCEENUMSIZEINT = 2147483647
	};

	inline void *__cdecl RwIm3DTransform( RwIm3DVertex *pVerts, std::uint32_t numVerts, RwMatrix *ltm = nullptr, std::uint32_t flags = 9 ) {
		static constexpr auto kRwIm3DTransformAddr = 0x7EF450;
		auto fn = reinterpret_cast<decltype( &RwIm3DTransform )>( kRwIm3DTransformAddr );

		return fn( pVerts, numVerts, ltm, flags );
	}

	inline bool __cdecl RwIm3DRenderPrimitive( RwPrimitiveType primTyp ) {
		static constexpr auto kRwIm3DRenderPrimitiveAddr = 0x7EF6B0;
		auto fn = reinterpret_cast<decltype( &RwIm3DRenderPrimitive )>( kRwIm3DRenderPrimitiveAddr );

		return fn( primTyp );
	}

	inline bool __cdecl RwIm3DEnd() {
		static constexpr auto kRwIm3DEndAddr = 0x7EF520;
		auto fn = reinterpret_cast<decltype( &RwIm3DEnd )>( kRwIm3DEndAddr );

		return fn();
	}

	struct RwImage {};

	inline RwImage *__cdecl RtPNGImageRead( const char *path ) {
		static constexpr auto kRtPNGImageReadAddr = 0x7CF9B0;
		auto fn = reinterpret_cast<decltype( &RtPNGImageRead )>( kRtPNGImageReadAddr );

		return fn( path );
	}

	inline bool __cdecl RwImageDestroy( RwImage *mage ) {
		static constexpr auto kRwImageDestroyAddr = 0x802740;
		auto fn = reinterpret_cast<decltype( &RwImageDestroy )>( kRwImageDestroyAddr );

		return fn( mage );
	}

	enum RwRasterType {
		rwRASTERTYPENORMAL = 0,
		rwRASTERTYPEZBUFFER,
		rwRASTERTYPECAMERA,
		rwRASTERTYPETEXTURE,
		rwRASTERTYPECAMERATEXTURE,
		rwRASTERTYPEMASK,
		rwRASTERDONTALLOCATE = 128,
		rwRASTERTYPEFORCEENUMSIZEINT = 2147483647
	};

	inline const RwImage *__cdecl RwImageFindRasterFormat( const RwImage *ipImage,
														   RwRasterType nRasterType,
														   int &npWidth,
														   int &npHeight,
														   int &npDepth,
														   int &npFormat ) {
		static constexpr auto kRwImageFindRasterFormatAddr = 0x8042C0;
		auto fn = reinterpret_cast<decltype( &RwImageFindRasterFormat )>( kRwImageFindRasterFormatAddr );

		return fn( ipImage, nRasterType, npWidth, npHeight, npDepth, npFormat );
	}

	struct RwRaster {};

	inline RwRaster *__cdecl RwRasterCreate( int width, int height, int depth, int flags ) {
		static constexpr auto kRwRasterCreateAddr = 0x7FB230;
		auto fn = reinterpret_cast<decltype( &RwRasterCreate )>( kRwRasterCreateAddr );

		return fn( width, height, depth, flags );
	}

	inline RwRaster *__cdecl RwRasterSetFromImage( RwRaster *raster, const RwImage *image ) {
		static constexpr auto kRwRasterSetFromImageAddr = 0x804290;
		auto fn = reinterpret_cast<decltype( &RwRasterSetFromImage )>( kRwRasterSetFromImageAddr );

		return fn( raster, image );
	}

	inline bool __cdecl RwRasterDestroy( RwRaster *raster ) {
		static constexpr auto kRwRasterDestroyAddr = 0x7FB020;
		auto fn = reinterpret_cast<decltype( &RwRasterDestroy )>( kRwRasterDestroyAddr );

		return fn( raster );
	}

	struct RpWorld {};

	using aStdBonePosisions_t = CVector[303];
	inline aStdBonePosisions_t &aStdBonePosisions = *reinterpret_cast<aStdBonePosisions_t *>( 0x8D13A8 );

	struct CPad {
		static inline CPad *__cdecl GetPad( std::int32_t nPlayer = 0 ) {
			static constexpr auto kGetPadAddr = 0x53FB70;
			auto fn = reinterpret_cast<decltype( &GetPad )>( kGetPadAddr );

			return fn( nPlayer );
		}

		inline bool GetTarget() const {
			static constexpr auto kGetTargetAddr = 0x540670;
			using fn_t = bool( __thiscall * )( const CPad * );
			auto fn = reinterpret_cast<fn_t>( kGetTargetAddr );

			return fn( this );
		}

		inline std::int32_t GetWeapon( CPlayerPed *pPlayerPed ) const {
			static constexpr auto kGetWeaponAddr = 0x540180;
			using fn_t = std::int32_t( __thiscall * )( const CPad *, CPlayerPed * );
			auto fn = reinterpret_cast<fn_t>( kGetWeaponAddr );

			return fn( this, pPlayerPed );
		}
	};
} // namespace game