#include "hooks.h"

#include <filesystem>
#include <iterator>
#include <limits>
#include <string>

#include <windows.h>

#include "game.hpp"

EXTERN_C IMAGE_DOS_HEADER __ImageBase; // NOLINT

namespace {
	constexpr std::uint32_t kPedLimits[] = { 30, 50, 70, 90, 120, 150 };
	constexpr float kRenderDistances[] = { 70.0f, 50.0f, 35.0f, 20.0f, 10.0f, 5.0f };
	static_assert( std::size( kPedLimits ) == std::size( kRenderDistances ) );

	constexpr std::size_t kInvalidPedIndex = std::numeric_limits<std::size_t>::max();

	std::size_t get_ped_index( const game::CPed *ped ) {
		auto *pool = game::CPool<game::CPed>::get();
		if ( pool == nullptr || ped < pool->m_aStorage ) return kInvalidPedIndex;

		const auto ped_size = pool->entry_size();
		if ( ped_size == 0 ) return kInvalidPedIndex;

		auto index = ( reinterpret_cast<std::uintptr_t>( ped ) - reinterpret_cast<std::uintptr_t>( pool->m_aStorage ) ) / ped_size;
		if ( index >= pool->m_nSize ) return kInvalidPedIndex;

		return index;
	}

	std::filesystem::path plugin_path() {
		wchar_t buffer[MAX_PATH];
		GetModuleFileNameW( reinterpret_cast<HMODULE>( &__ImageBase ), buffer, MAX_PATH );
		return std::filesystem::path( buffer );
	}

	struct ped_data {
		std::uint32_t frame_id = 0;
		bool need_process = true;
	};

	// FIXME: May not call std::vector destructor
	struct peds_data : std::vector<ped_data> {
		peds_data() : std::vector<ped_data>( game::CPool<game::CPed>::kDefaultPoolSize ) {}

		ped_data &operator[]( std::size_t index ) {
			if ( index >= size() ) {
				resize( index + 1 );
			}
			return at( index );
		}
	};

	struct attach_bak {
		game::CPed *ped = nullptr;
		std::uintptr_t vtbl = 0;
	};

	struct player_pad {
		bool is_target : 1;
		bool is_attack : 1;

		player_pad() : is_target( false ), is_attack( false ) {}

		[[nodiscard]] bool is_target_or_attack() const { return is_target || is_attack; }
	};

	struct plugin {
		plugin() {
			hooks::is_need_process_ped = [this]( game::CPed *ped ) {
				return is_need_process_ped( ped );
			};
			hooks::on_render = [this]() {
				return render();
			};
			hooks::on_before_update_attachments = [this]( game::CPed *ped ) {
				return before_update_attachments( ped );
			};
			hooks::on_after_update_attachments = [this]() {
				return after_update_attachments();
			};
			hooks::update_player_pad = [this]( game::CPlayerPed *player ) {
				return update_player_pad( player );
			};
		}

		~plugin() {
			if ( image != nullptr ) game::RwRasterDestroy( image );
		}

	protected:
		bool initialized = false;
		peds_data peds;
		std::uint32_t frame_id = 0;
		std::uint32_t ped_counter = 0;
		float render_dist = kRenderDistances[0];
		std::vector<game::RwIm3DVertex> vertices;
		game::RwRaster *image = nullptr;
		std::vector<attach_bak> attach_baks;
		std::vector<player_pad> player_pads;

		bool initialize() {
			if ( initialized ) return true;
			if ( game::CPool<game::CPed>::get()->entry_size() == 0 ) return false;

			peds.resize( game::CPool<game::CPed>::get()->m_nSize );
			// NOLINTNEXTLINE(*magic-numbers): 6 is count vertices per player
			vertices.reserve( game::CPool<game::CPed>::get()->m_nSize * 6 );
			attach_baks.reserve( game::CPool<game::CPed>::get()->m_nSize );
			player_pads.resize( game::CPool<game::CPed>::get()->m_nSize );
			initialized = true;
			return true;
		}

		float calc_render_dist() {
			for ( auto i = 0; i < std::size( kPedLimits ); i++ ) {
				if ( ped_counter < kPedLimits[i] ) return kRenderDistances[i];
			}

			return kRenderDistances[std::size( kRenderDistances ) - 1];
		}

		game::RwRaster *ped_texture() {
			if ( image != nullptr ) return image;

			auto image_path = plugin_path().replace_extension( L".png" );
			if ( !std::filesystem::exists( image_path ) ) return image;

			auto image_path_utf16 = image_path.wstring();
			int size_needed = WideCharToMultiByte( CP_ACP, 0, image_path_utf16.c_str(), -1, nullptr, 0, nullptr, nullptr );
			std::string image_path_acp( size_needed, '\0' );
			WideCharToMultiByte( CP_ACP, 0, image_path_utf16.c_str(), -1, image_path_acp.data(), size_needed, nullptr, nullptr );

			auto *png = game::RtPNGImageRead( image_path_acp.c_str() );
			if ( png == nullptr ) return image;

			int width;
			int height;
			int depth;
			int format;
			game::RwImageFindRasterFormat( png, game::rwRASTERTYPECAMERATEXTURE, width, height, depth, format );

			auto *raster = game::RwRasterCreate( width, height, depth, format );
			if ( raster == nullptr ) return image;

			game::RwRasterSetFromImage( raster, png );
			game::RwImageDestroy( png );

			image = raster;
			return image;
		}

		bool is_need_process_ped( game::CPed *ped ) {
			const auto *local_ped = game::CPlayerPed::local();
			if ( !initialize() || local_ped == ped ) return true;

			if ( local_ped == nullptr || local_ped->m_pMat == nullptr || local_ped->IsDead() ) return true;

			auto index = get_ped_index( ped );
			if ( index == kInvalidPedIndex ) return true;

			auto &player = peds[index];
			if ( player.frame_id == frame_id ) return player.need_process;

			ped_counter++;
			player.need_process = ( local_ped->GetPosition() - ped->GetPosition() ).MagnitudeSqr() < render_dist * render_dist;
			player.need_process |= player_pads[index].is_target_or_attack() && !ped->IsArmsMeleeWeapon();
			if ( !local_ped->IsArmsMeleeWeapon() ) {
				auto local_index = get_ped_index( local_ped );
				player.need_process |= local_index != kInvalidPedIndex && player_pads[local_index].is_target_or_attack() && ped->IsPlayer();
			}
			player.frame_id = frame_id;

			if ( !player.need_process && ped->m_pMat != nullptr ) {
				auto lu = ped->m_pMat->Project( game::CVector( -0.25f, 0.0f, 0.75f ) );
				auto ru = ped->m_pMat->Project( game::CVector( 0.25f, 0.0f, 0.75f ) );
				auto lb = ped->m_pMat->Project( game::CVector( -0.25f, 0.0f, -1.0f ) );
				auto rb = ped->m_pMat->Project( game::CVector( 0.25f, 0.0f, -1.0f ) );

				vertices.emplace_back( lu, game::CVector{}, game::kRwRGBAWhite, game::RwTexCoords{ 0.0f, 0.0f } );
				vertices.emplace_back( ru, game::CVector{}, game::kRwRGBAWhite, game::RwTexCoords{ 1.0f, 0.0f } );
				vertices.emplace_back( lb, game::CVector{}, game::kRwRGBAWhite, game::RwTexCoords{ 0.0f, 1.0f } );
				vertices.emplace_back( lb, game::CVector{}, game::kRwRGBAWhite, game::RwTexCoords{ 0.0f, 1.0f } );
				vertices.emplace_back( ru, game::CVector{}, game::kRwRGBAWhite, game::RwTexCoords{ 1.0f, 0.0f } );
				vertices.emplace_back( rb, game::CVector{}, game::kRwRGBAWhite, game::RwTexCoords{ 1.0f, 1.0f } );
			}

			return player.need_process;
		}

		void render() {
			if ( !initialize() ) return;

			if ( !vertices.empty() ) {
				auto cull_mode = game::GetRenderState<game::RwCullMode>( game::rwRENDERSTATECULLMODE );
				game::SetRenderState( game::rwRENDERSTATECULLMODE, game::rwCULLMODECULLNONE );
				auto *raster = game::GetRenderState<game::RwRaster *>( game::rwRENDERSTATETEXTURERASTER );
				game::SetRenderState( game::rwRENDERSTATETEXTURERASTER, ped_texture() );
				if ( game::RwIm3DTransform( vertices.data(), vertices.size() ) != nullptr ) {
					game::RwIm3DRenderPrimitive( game::rwPRIMTYPETRILIST );
					game::RwIm3DEnd();
				}
				game::SetRenderState( game::rwRENDERSTATETEXTURERASTER, raster );
				game::SetRenderState( game::rwRENDERSTATECULLMODE, cull_mode );
				vertices.clear();
			}

			render_dist = calc_render_dist();
			ped_counter = 0;
			frame_id = frame_id == std::numeric_limits<decltype( frame_id )>::max() ? 0 : frame_id + 1;
		}

		void before_update_attachments( game::CPed *ped ) {
			if ( is_need_process_ped( ped ) ) return;

			attach_baks.emplace_back( ped, ped->vtbl );
			ped->vtbl = game::CPlaceable::kVtblAddr;
		}

		void after_update_attachments() {
			for ( auto &bak : attach_baks ) {
				bak.ped->vtbl = bak.vtbl;
			}
			attach_baks.clear();
		}

		void update_player_pad( game::CPlayerPed *player ) {
			auto index = get_ped_index( player );
			if ( index == kInvalidPedIndex ) return;

			const auto *pad = game::CPad::GetPad();
			player_pads[index].is_target = pad->GetTarget();
			player_pads[index].is_attack = pad->GetWeapon( player ) != 0;
		}
	} plugin;
} // namespace